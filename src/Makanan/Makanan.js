import React, { Component } from 'react';
import { FormControl, Button, Table, Row, Col} from 'react-bootstrap';

// import store from '../Reducers/store';

export default class Makanan extends Component {
	constructor() {
		super();
		this.state = {
			filterFood : ''
		}

		// this.goFilter = this.goFilter.bind(this);
		this.filterMakanan = this.filterMakanan.bind(this);
	}

	filterMakanan(event) {
		this.setState({
			filterFood: event.target.value
		});
	}

	render() {
		// properti yang didapat atau dipassing dari component MakananContainer (hasil redux)
		let panganan = this.props.data.namaMakanan.food;

		if (this.state.filterFood) {
			panganan = panganan.filter((maeman) => {
				let fullName = `${maeman.nama_makanan}`;
				return fullName.toLowerCase().includes(this.state.filterFood.toLowerCase());
			});
		}

		return(
			<div>
				<Row className="show-grid">
				    <Col md={2} />

				    <Col md={8} >

						<FormControl
				            type="text"
				            placeholder="Cari makanan"
				            onChange={this.filterMakanan}
				        />
		    			<Table responsive>
		    			  <thead>
		    			    <tr>
		    			      <th>#</th>
		    			      <th>Nama Makanan</th>
		    			      <th>Harga</th>
		    			      <th>Detail</th>
		    			    </tr>
		    			  </thead>
		    			  <tbody>
		    			  	{ panganan.map((efuuud, key) => 
		    	  		  		<tr key={ key }>
		    	  			      <td>{ key+1 }</td>
		    	  			      <td>{ efuuud.nama_makanan}</td>
		    	  			      <td>{ efuuud.harga_makanan}</td>
		    	  			      <td>  <Button bsStyle="primary" href={ "makanan/detail/" + efuuud.id }>Detail</Button> </td>
		    	  			    </tr>
		    			  	)}
		    			  </tbody>
		    			</Table>
				    </Col>
				    <Col md={2} />
				 </Row>
			</div>
		);
	}
}