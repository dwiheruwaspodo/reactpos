import React, { Component } from 'react';

// import redux
import { connect } from 'react-redux';

// import komponen makanan
import Makanan from './Makanan';

// import reducer store
import store from '../Reducers/store';

// import data makanan
import food from "./DataFood";

class MakananContainer extends Component {
	componentWillMount() {
		store.dispatch({
			type: 'GET_FOOD',
			food: food
		});
	}

	render() {
		return <Makanan {...this.props} />
	}
}

//CONNECT REDUX TO REACT
const mapStateToProps = state => {
	return {data: state}
}


// export default connect(mapStateToProps)(Makanan);
export default connect(mapStateToProps)(MakananContainer);