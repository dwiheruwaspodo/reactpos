import React from 'react';
import { Route, IndexRoute, Redirect } from "react-router";

import KursContainer from '../Kurs/KursContainer';
import Maxx from '../Maxx/Maxx';
import MakananContainer from '../Makanan/MakananContainer';
import Minuman from '../Minuman/Minuman';
import {ConnectedTodo} from '../Todo/Todo';
import MakananDetail from '../Makanan/MakananDetail';
import Master from '../Master';
import PageNotFound from "../PageNotFound";
import Home from "../Home";

// model ginian karena langsung ngembaliin halaman nggak ada return , kalo modelnya return cuma ngembaliin satu doang (karena berupa fungsi)
export const routes = (
	<Route path="/" component={Master}>
		{/* index routes */}
		<IndexRoute component={Home} />
		<Route path="home" component={Home} />
		<Route path="todo" component={ConnectedTodo} />
		<Route path="berita" component={KursContainer} />
		<Route path="maxx" component={Maxx} />
		{/* nested routes */}
		<Route path="makanan">
			<IndexRoute component={MakananContainer} />
			<Route path="detail/:id" component={MakananDetail}/>
		</Route>
		<Route path="minuman">
			<IndexRoute component={Minuman} />
		</Route>
		{/* redirect*/}
		<Redirect from="users(/:id)" to="daftarnama(/:id)" />

		{/* Page not found*/}
		<Route path="*" component={PageNotFound} />
	</Route>
);