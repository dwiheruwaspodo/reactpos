import React from 'react';
import ReactDOM from 'react-dom';

import registerServiceWorker from './registerServiceWorker';
import { Router, browserHistory } from "react-router";

// import routes
import { routes } from './Routes/routes';

// import redux
// nggak pake kurung kurawal karena udah di definisikan default di komponen store
import store from "./Reducers/store";
import { Provider } from 'react-redux';

ReactDOM.render(
	// harus diassign ke provider
	<Provider store={ store }>
		<Router history={ browserHistory }>
			{ routes }
		</Router>
	</Provider>,
	document.getElementById('root')
);
registerServiceWorker();
