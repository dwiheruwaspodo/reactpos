import React, { Component } from 'react';

// import { FormControl, Button, Table, Row, Col} from 'react-bootstrap';
import { connect } from 'react-redux';
// import store from '../Reducers/store';
import { listKurs } from './actions';
import Kurs from "./Kurs";

class KursContainer extends Component {
	constructor() {
		super();
		// eksekusi redux
		listKurs()
	}

	componentDidMount() {
		// console.log(listKurs());
	}

	render() {
		return (
			<Kurs {...this.props } />
		);
	}
}

const mapStateToProps = state => {
	return {data: state}
}

export default connect(mapStateToProps)(KursContainer);