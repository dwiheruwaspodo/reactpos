import store from '../Reducers/store';

// SUCCESS
function fetchPostsSuccess(payload) {
	store.dispatch({
  		type: "GET_KURS",
	    kurs: payload
	})
}

// ERROR
function fetchPostsError() {
  	store.dispatch({
    	type: "FETCH_ERROR"
  	})
}

// INI KENA PROMISE NGGAK TAU SOLVE NYA
// const listKursAneh = (dispatch) => (
//     fetchPosts().then(([response, json]) =>{
//     	if(response.status === 200){
//       		return fetchPostsSuccess(json);
//       	}
//       	else{
//       		return fetchPostsError()
//       	}
//     })
// );

// function fetchPosts() {
//   const URL = "https://jsonplaceholder.typicode.com/posts";
//   return fetch(URL, { method: 'GET'})
//      .then( response => Promise.all([response, response.json()]));
//      .then( response => response.text());
// }


// AKSES API
const listKurs = (dispatch) => {
    fetch("https://jsonplaceholder.typicode.com/posts", { method: 'GET'})
    // .then(response => response.text())   
    .then(response => response.json())      
    .then(json => {
    	return fetchPostsSuccess(json);
    })
    .then(([response, json]) =>{
    	if(response.status === 200){
      		return fetchPostsSuccess(json);
      	}
      	else{
      		return fetchPostsError()
      	}
    })
    .catch(error => {                 
        return fetchPostsError();
    })
}

export { listKurs }