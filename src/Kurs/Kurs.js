import React, { Component } from 'react';

import { Table, Row, Col} from 'react-bootstrap';

export default class Kurs extends Component {
	render() {
		// properti_yg_didapat_dari.data(dari_kurs_continer).array_berita(disetdi_store_js).array_yangdiset_diredux
		let berita = this.props.data.berita.kurs;

		return (
			<div>
				<Row className="show-grid">
				    <Col md={2} />

				    <Col md={8} >

						<Table responsive>
		    			  <thead>
		    			    <tr>
		    			      <th>#</th>
		    			      <th> Title </th>
		    			    </tr>
		    			  </thead>
		    			  <tbody>
		    			  	{
		    			  		berita.map((beritane, key) => 
    			  			  		<tr key={ key }>
    			  				      <td>{ key+1 }</td>
    			  				      <td>{ beritane.title}</td>
    			  				    </tr>
		    			  		)
		    			  	}
		    			  </tbody>
		    			</Table>
				    </Col>
				    <Col md={2} />
				 </Row>
			</div>
		);
	}
}