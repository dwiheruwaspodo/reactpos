import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Button, Table, Row, Col} from 'react-bootstrap';
import { addTodo, finishTodo } from './actions'

// import store dari reducer
import store from "../Reducers/store";

//VIEW
class Todo extends Component {
	constructor() {
		super();

		this.addTodo = this.addTodo.bind(this);
	}

	addTodo() {
		store.dispatch(addTodo())
	}

	finished(id) {
		store.dispatch(finishTodo(id))
	}

	render() {
		let todos = this.props.data.map( (item, index) => (
			<tr key={ index }>
	      		<td>{ index+1 }</td>
	      		<td>
	      			<div className={'todos'}>
						<span onClick={() => this.finished(item.id)} className={item.completed ? 'striked' : ''}>
							{item.text}
						</span>
					</div>
	      		</td>
	    	</tr>
		))

		return (
			<div>
				<Row className="show-grid">
				    <Col md={2} />

				    <Col md={8} >
		    			<Button bsStyle="primary" onClick={ this.addTodo }>Add to do</Button>
						<Table responsive>
		    			  	<thead>
		    			    	<tr>
		    			      		<th>#</th>
		    			      		<th>To Do</th>
		    			    	</tr>
		    			  	</thead>
		    			  	<tbody>
	    	  		  			{todos}
		    			  </tbody>
		    			</Table>
				    </Col>
				    <Col md={2} />
				</Row>
			</div>
		);
	}
}

// CONNECT REDUX TO REACT
// OTOMATIS BIKIN PROPERTI
const mapStateToProps = state => {
	return { data: state.todoReducer }
}
export const ConnectedTodo = connect(mapStateToProps)(Todo);