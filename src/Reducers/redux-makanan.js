const initialNameState = {
	food: []
}

const namaMakanan = (state = initialNameState, action) => {
	switch (action.type) {
		case 'GET_FOOD' :
			return Object.assign({}, state, {food: action.food});
		default :
			return state;
	}
}
export default namaMakanan;