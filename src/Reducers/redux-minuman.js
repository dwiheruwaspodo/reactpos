const initDrinks = {
	drink: []
}

const namaMinuman = (state = initDrinks, action) => {
	switch (action.type) {
		case 'GET_DRINKS' :
			return Object.assign({}, state, {drink: action.drink});
		default :
			return state;
	}
}

export default namaMinuman;