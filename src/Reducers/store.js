import { createStore, combineReducers } from 'redux'

// import redux dari makanan
import namaMakanan from './redux-makanan'; 
// import redux dari minuman
import namaMinuman from './redux-minuman'; 
// import redux dari todo
import todoReducer from './redux-todo'; 
import kursUpdate from './redux-kurs'; 
import maxxReducers from './redux-maxx'; 


let kumpulanReducer = combineReducers({
	namaMinuman: namaMinuman,
	namaMakanan: namaMakanan,
	todoReducer: todoReducer,
	berita: kursUpdate,
	maxx: maxxReducers

});

// di export untuk dipake disemua komponen
let store = createStore(kumpulanReducer);

export default store;
