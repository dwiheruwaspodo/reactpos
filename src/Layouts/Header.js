import React, { Component } from "react";

import { Navbar, NavItem, Nav } from 'react-bootstrap';
// import { Link, IndexLink } from 'react-router';
export default class Header extends Component {
	constructor(props, context) {
	    super(props, context);

	    this.handleSelect = this.handleSelect.bind(this);

	    this.state = {
	      key: 0
	    };
	}

	handleSelect(key) {
		// alert(`selected ${key}`);
		// console.log(...this.props)
	    this.setState({ key: key });
	}

	    
   	// componentWillUpdate(nextProps, nextState) {
    //   	console.log('Component WILL UPDATE!');
   	// }

	render() {
		// let act = this.state.key;
		// console.log(act);
		return(
			<Navbar inverse collapseOnSelect onSelect={k => this.handleSelect(k)}>
			  <Navbar.Header>
			    <Navbar.Brand>
			      <a href="/">React-POS</a>
			    </Navbar.Brand>
			  </Navbar.Header>
			  <Nav>
			    <NavItem eventKey={1} href="/todo">
			      To Do
			    </NavItem>
			    <NavItem eventKey={2} href="/makanan">
			      Menu makanan
			    </NavItem>
			    {/*<Link to="/makanan" activeKey={true}> Food </Link>*/}
			    <NavItem eventKey={3} href="/minuman" >
			      Menu minuman
			    </NavItem>
			    <NavItem eventKey={4} href="/berita" >
			      Berita
			    </NavItem>
			    <NavItem eventKey={5} href="/maxx" >
			      Maxx
			    </NavItem>
			  </Nav>
			  <Navbar.Text pullRight>Have a great day, cuks!</Navbar.Text>
			</Navbar>
		);
	}
}