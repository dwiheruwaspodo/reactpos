import React, { Component } from 'react';

import { Navbar, NavItem, Nav } from 'react-bootstrap';

export default class Navlink extends Component {
	constructor() {
		super();
		this.clickHandler = this.clickHandler.bind(this);
	}

	clickHandler() {
		var active = "active";
	}

	render() {
		const style = {
			backgroundColor: 'blue',
			color: 'white',
			fontSize: '2em'
		}
		return (
			<NavItem {...this.props} onClick={this.clickHandler} activeKey={true}/>
		)
		
	}

}